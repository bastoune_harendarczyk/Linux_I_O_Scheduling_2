#!/bin/bash

#---------------------------------------------------Microbenchmarks-Streaming-Writes-and-Reads--------------------------------------------------#

cd Microbenchmarks-Streaming-Writes-and-Reads

echo deadline > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nDEADLINE\n"

./script.sh &
sleep 5
./script2.sh 
sleep 5
./script2.sh 
sleep 5
./script2.sh 
wait
echo -e "\n"


echo noop > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nNOOP\n"

./script.sh &
sleep 5
./script2.sh 
sleep 5
./script2.sh 
sleep 5
./script2.sh 
wait
echo -e "\n"

echo cfq > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nCFQ\n"

./script.sh &
sleep 5
./script2.sh 
sleep 5
./script2.sh 
sleep 5
./script2.sh 
wait
echo -e "\n"

#-----------------------------------------------------------------------------------------------------------------------------------------------#
